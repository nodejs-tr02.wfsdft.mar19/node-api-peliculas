const express = require('express');
var bodyParser = require('body-parser');

const app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false})); // Para formularios


app.get('/',(req,res) => {
    res.send("Hola Clase!!")
});

app.listen(3000);

// get a ruta movie devuelve json con info pelicula

let fakeMovie = [
    { 
      "nombre"  :  "El señor de los anillos", 
      "genero"   :  "Accion",
      "año" : "2010"
    }]

let fakeDB  = {
    '1': {
        "nombre"  :  "El señor de los anillos", 
        "genero"   :  "Accion",
        "año" : "2010"},
    '2': {
        "nombre"  :  "Casablanca", 
        "genero"   :  "Suspense",
        "año" : "1949" 
    },
    '3': {
        "nombre"  :  "Casablanca 2", 
        "genero"   :  "Comedia",
        "año" : "1900" 
    },
    '4': {
        "nombre"  :  "Regreso al Futuro", 
         "genero"   :  "Accion",
         "año" : "1982" 
    },
    '5': {
        "nombre"  :  "El Padrino", 
         "genero"   :  "Drama",
         "año" : "1971" 
    },
    '6': {
        "nombre"  :  "Bla Bla Bla", 
         "genero"   :  "Drama",
         "año" : "1971" 
    },
    '7': {
        "nombre"  :  "Sherlock Holmes", 
         "genero"   :  "Suspense",
         "año" : "2010" 
    },
    '8': {
        "nombre"  :  "Blade Runner", 
         "genero"   :  "Ciencia Ficción",
         "año" : "1979" 
    }
}

//fakeDB['1'];

app.get('/movie',(req,res) => {
        res.send(fakeDB)
    });


app.post('/movie',(req,res) => {
        console.log(req.body);
        let peli = req.body;
        //fakeDB = {...fakeDB, peli}
        id = Object.keys(fakeDB).length +1;
        fakeDB[id] = peli;
        res.send(fakeDB);
    });


app.get('/movie/:id',(req,res) => {
        console.log(req.params.id);
        let id = req.params.id;
        res.send(fakeDB[id]);
    });

app.put('/movie/:id',(req,res) => {
        console.log(req.params.id);
        let id = req.params.id;
        let peli = req.body;
        fakeDB[id] = peli;
        res.send(fakeDB);
    });

app.delete('/movie/:id',(req,res) => {
        console.log(req.params.id);
        let id = req.params.id;
        delete fakeDB[id];
        res.send(fakeDB);
    });

//req.params.id === 25
//app.get('/movie/:id',(req,res))
//GET /movie -> devuelve todas las peliculas
//GET /movie/25 -> pelicula con id 25
//POST /movie -> guardar nueva pelicula
//PUT /movie/25 -> modifica
//DELETE /movie/25 -> borramos peli 25

//var query1=req.body.inputname;